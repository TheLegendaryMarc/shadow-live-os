#!/bin/bash

set -e

script_path=$(readlink -f ${0%/*})

path="${script_path}/aur"
db_path="${path}/x86_64"

# Delete the existing content
rm -rf ${db_path}

mkdir -p ${path}
mkdir -p ${db_path}

# Read each packages one by one
grep -v ^# <${script_path}/packages.aur | while read package || [ -n "${package}" ] ; do
    if [ -d "${path}/${package}" ]; then
        cd "${path}/${package}"
        git pull
    else
        cd ${path}
        git clone "https://aur.archlinux.org/${package}.git"
        cd ${package}
    fi

    # Build package
    makepkg --syncdeps --noconfirm --cleanbuild

    # Move package to the database root
    mv *.pkg.tar.xz ${db_path}
done

cd $db_path
# Add the packages to the local repository
repo-add aur.db.tar.gz *.pkg.tar.xz
