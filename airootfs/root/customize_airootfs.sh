#!/bin/bash

set -e -u

# rm -rf /usr/lib/modules/* lines are commented-out to preven warning/errors
# during build

_remove_needless_firmwares_netronome() {
    # Removing netronome firmwares because of the low encounter chance
    # It's part of the linux-firmware package
    # See https://bugs.archlinux.org/task/60568 for side information

    # Found theses locations with
    # find -iname '*netronome*'
    rm -rf /usr/lib/firmware/netronome
    rm -rf /usr/share/licenses/linux-firmware/LICENCE.Netronome
    #rm -rf /usr/lib/modules/4.19.4-arch1-1-ARCH/kernel/drivers/net/ethernet/netronome
}

_remove_needless_firmwares_liquidio() {
    # Removing liquidio firmwares because of the low encounter chance
    # It's part of the linux-firmwares package
    # LiquidIO are high-performance network cards

    # Found theses locations with
    # find -iname '*liquidio*'
    rm -rf /usr/lib/firmware/liquidio
    rm -rf /usr/share/licenses/linux-firmware/LICENCE.cavium_liquidio
    #rm -rf /usr/lib/modules/4.19.4-arch1-1-ARCH/kernel/drivers/net/ethernet/cavium/liquidio
}

_remove_needless_firmwares_qed() {
    # Removing qed firmwares because of the low encounter chance
    # It's part of the linux-firmwares package
    # QLogic is a high-performance network cards manufacturer. The firmwares
    # for their cards are labelled "qed".

    # Found theses locations with
    # find -iname '*qed*'
    rm -rf /usr/lib/firmware/qed
    #rm -rf /usr/lib/modules/4.19.4-arch1-1-ARCH/kernel/drivers/net/ethernet/qlogic/qed
}

_remove_needless_firmwares_crumbs() {
    (
        grep -v '^#' | while read f ; do
            if [ -n "$f" ] ; then
                rm -rf "$f"
            fi
        done
    ) <<EOF
# phanfw is the driver for a high-performances ethernet card from NetXen
/usr/lib/firmware/phanfw.bin
/usr/share/licenses/linux-firmware/LICENCE.phanfw
# ti-connectivity WiFi chips for embedded systems have a low encounter rate
# for Shadow use
/usr/lib/firmware/ti-connectivity/wl1251-fw.bin
/usr/lib/firmware/ti-connectivity/wl1251-nvs.bin
/usr/share/licenses/linux-firmware/LICENCE.wl1251
/usr/lib/firmware/ti-connectivity/wl1271-fw-2.bin
/usr/lib/firmware/ti-connectivity/wl1271-fw-ap.bin
/usr/lib/firmware/ti-connectivity/wl1271-fw.bin
/usr/lib/firmware/ti-connectivity/wl1271-nvs.bin
/usr/lib/firmware/ti-connectivity/wl127x-fw-3.bin
/usr/lib/firmware/ti-connectivity/wl127x-fw-4-mr.bin
/usr/lib/firmware/ti-connectivity/wl127x-fw-4-plt.bin
/usr/lib/firmware/ti-connectivity/wl127x-fw-4-sr.bin
/usr/lib/firmware/ti-connectivity/wl127x-fw-5-mr.bin
/usr/lib/firmware/ti-connectivity/wl127x-fw-5-plt.bin
/usr/lib/firmware/ti-connectivity/wl127x-fw-5-sr.bin
/usr/lib/firmware/ti-connectivity/wl127x-fw-plt-3.bin
/usr/lib/firmware/ti-connectivity/wl127x-nvs.bin
/usr/lib/firmware/ti-connectivity/wl128x-fw-3.bin
/usr/lib/firmware/ti-connectivity/wl128x-fw-4-mr.bin
/usr/lib/firmware/ti-connectivity/wl128x-fw-4-plt.bin
/usr/lib/firmware/ti-connectivity/wl128x-fw-4-sr.bin
/usr/lib/firmware/ti-connectivity/wl128x-fw-5-mr.bin
/usr/lib/firmware/ti-connectivity/wl128x-fw-5-plt.bin
/usr/lib/firmware/ti-connectivity/wl128x-fw-5-sr.bin
/usr/lib/firmware/ti-connectivity/wl128x-fw-ap.bin
/usr/lib/firmware/ti-connectivity/wl128x-fw.bin
/usr/lib/firmware/ti-connectivity/wl128x-fw-plt-3.bin
/usr/lib/firmware/ti-connectivity/wl128x-nvs.bin
/usr/lib/firmware/ti-connectivity/wl12xx-nvs.bin
/usr/lib/firmware/ti-connectivity/wl18xx-fw-2.bin
/usr/lib/firmware/ti-connectivity/wl18xx-fw-3.bin
/usr/lib/firmware/ti-connectivity/wl18xx-fw-4.bin
/usr/lib/firmware/ti-connectivity/wl18xx-fw.bin
#/usr/lib/modules/4.19.4-arch1-1-ARCH/kernel/drivers/media/radio/radio-wl1273.ko.xz
#/usr/lib/modules/4.19.4-arch1-1-ARCH/kernel/drivers/media/radio/wl128x
#/usr/lib/modules/4.19.4-arch1-1-ARCH/kernel/drivers/mfd/wl1273-core.ko.xz
#/usr/lib/modules/4.19.4-arch1-1-ARCH/kernel/drivers/net/wireless/ti/wl1251
#/usr/lib/modules/4.19.4-arch1-1-ARCH/kernel/drivers/net/wireless/ti/wl18xx
# Removing ti-connectivity bluetooth drivers as well
/usr/lib/firmware/ti-connectivity/TIInit_6.2.31.bts
/usr/lib/firmware/ti-connectivity/TIInit_6.6.15.bts
/usr/lib/firmware/ti-connectivity/TIInit_7.2.31.bts
# At this point we should have nothing left from ti-connectivity, removing
# the directory and LICENCE file, just to be clean
/usr/lib/firmware/ti-connectivity
/usr/lib/firmware/LICENCE.ti-connectivity
# Adreno is a GPU made by Qualcomm and found on Snapdragon SoC, which are ARM
# SoC, so unusable with Shadow. Thus, removing firmwares.
/usr/lib/firmware/qcom/a300_pfp.fw
/usr/lib/firmware/qcom/a300_pfp.fw
/usr/lib/firmware/qcom/qcom/a300_pm4.fw
/usr/lib/firmware/qcom/a300_pm4.fw
/usr/lib/firmware/qcom/qcom/a530_pfp.fw
/usr/lib/firmware/qcom/qcom/a530_pm4.fw
/usr/lib/firmware/qcom/qcom/a530v3_gpmu.fw2
/usr/lib/firmware/qcom/qcom/a530_zap.b00
/usr/lib/firmware/qcom/qcom/a530_zap.b01
/usr/lib/firmware/qcom/qcom/a530_zap.b02
/usr/lib/firmware/qcom/qcom/a530_zap.mdt
# Venus seems to be a video codec. I can't find much information about it, but
# if think there is a very low chance that we need this. Thus, removing
# firmwares.
/usr/lib/qcom/venus-1.8/venus.mdt
/usr/lib/qcom/venus-1.8/venus.b00
/usr/lib/qcom/venus-1.8/venus.b01
/usr/lib/qcom/venus-1.8/venus.b02
/usr/lib/qcom/venus-1.8/venus.b03
/usr/lib/qcom/venus-1.8/venus.b04
/usr/lib/qcom/venus-4.2/venus.mdt
/usr/lib/qcom/venus-4.2/venus.b00
/usr/lib/qcom/venus-4.2/venus.b01
/usr/lib/qcom/venus-4.2/venus.b02
/usr/lib/qcom/venus-4.2/venus.b03
/usr/lib/qcom/venus-4.2/venus.b04
/usr/lib/qcom/venus-5.2/venus.mdt
/usr/lib/qcom/venus-5.2/venus.b00
/usr/lib/qcom/venus-5.2/venus.b01
/usr/lib/qcom/venus-5.2/venus.b02
/usr/lib/qcom/venus-5.2/venus.b03
/usr/lib/qcom/venus-5.2/venus.b04
/usr/lib/qcom/venus-5.2/venus.b05
/usr/lib/qcom/venus-5.2/venus.mbn
# At this point we should have nothing left from qualcomm, removing
# the directory and LICENCE file, just to be clean
/usr/lib/firmware/qcom
/usr/lib/firmware/LICENCE.qcom
EOF
}

_remove_needless_firmwares() {
    _remove_needless_firmwares_netronome
    _remove_needless_firmwares_liquidio
    _remove_needless_firmwares_qed
    _remove_needless_firmwares_crumbs
}

_remove_needless_man() {
    pacman -R -s --noconfirm man-db
    # Found theses locations with
    # find -name man
    rm -rf /usr/share/man
    rm -rf /usr/local/man
    rm -rf /usr/local/share/man
    rm -rf /var/cache/man
}

_remove_needless_doc() {
    rm -rf /usr/share/doc
    rm -rf /usr/share/gtk-doc
}

_remove_needless_others() {
    pacman -R -s --noconfirm texinfo
    rm -rf /usr/include
    rm -rf /usr/bin/gtk3-demo
    rm -rf /usr/bin/gtk3-demo-application
    # Removing GObject Introspection Repository files
    rm -rf /usr/share/gir-1.0
}

remove_needless() {
    _remove_needless_man
    _remove_needless_doc
    _remove_needless_firmwares
    _remove_needless_others
}

_optimize_space_python2() {
    if [ \! -e /usr/bin/python2 ] ; then
        rm -rf /usr/lib/python2.7
        return
    fi
    # Here we are removing all files needed for development purpose or modules
    # installations through pip
    # We are also compiling python core and site-packages files with
    # optimization level 2, and removing all source, compiled, and optimized
    # level 1 files.
    for p in $(find /usr/lib/python2.7 -regextype posix-basic \( -regex '.*/tests\{0,1\}' -type d \)) ; do
        rm -rf $p
    done
    python2 -OO -m compileall -q
    python2 -OO -m compileall -q /usr/lib/python2.7/site-packages
    for file_o in $(find /usr/lib/python2.7 -regextype posix-basic \( -regex '.*.pyo' -type f \)) ; do
        file_c="${file_o%.pyo}.pyc"
        file="${file_o%.pyo}.py"
        rm -f $file_c
        rm -f $file
    done
    # Removing the config file solely because we're doing the same for
    # python3. (And anyways, all includes are removed from /usr/includ)
    rm -rf /usr/lib/python2.7/config
}

_optimize_space_python3() {
    if [ \! -e /usr/bin/python3 ] ; then
        rm -rf /usr/lib/python3.7
        return
    fi
    # Here we are removing all files needed for development purpose or modules
    # installations through pip
    # We are also compiling python core and site-packages files with
    # optimization level 2, and removing all source, compiled, and optimized
    # level 1 files.
    for p in $(find /usr/lib/python3.7 -regextype posix-basic \( -regex '.*/tests\{0,1\}' -type d \)) ; do
        rm -rf $p
    done
    python3 -OO -m compileall -q
    python3 -OO -m compileall -q /usr/lib/python3.7/site-packages
    for file_o2 in $(find /usr/lib/python3.7 -regextype posix-basic \( -regex '.*\.opt-2\.pyc' -type f \)) ; do
        file_o1="${file_o2%.opt-2.pyc}.opt-1.pyc"
        file_c="${file_o2%.opt-2.pyc}.pyc"
        file=$(echo -n "${file_o2%.cpython-37.opt-2.pyc}.py" | sed 's/__pycache__\///')
        rm -f $file_o1
        rm -f $file_c
        rm -f $file
    done
    # I wonder why we have a config directory this big. The Arch packager
    # seriously fucked up IMO.
    rm -rf /usr/lib/python3.7/config-3.7m-x86_64-linux-gnu
}

_optimize_space_python() {
    echo 'export PYTHONOPTIMIZE=2' >> /etc/profile
    _optimize_space_python2
    _optimize_space_python3
}

optimize_space() {
    remove_needless
    _optimize_space_python
}

sed -i 's/#\(en_US\.UTF-8\)/\1/' /etc/locale.gen
locale-gen

# ln -sf /usr/share/zoneinfo/UTC /etc/localtime
ln -sf /usr/share/zoneinfo/Europe/Paris /etc/localtime

# Enable shadow login
echo "shadow ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers
useradd shadow -p "shadow" -g users -G "adm,audio,video,floppy,log,network,rfkill,scanner,storage,optical,power,wheel,input" -s /usr/bin/bash -k /etc/skel.shadow -m

# sed -i 's/#\(PermitRootLogin \).\+/\1yes/' /etc/ssh/sshd_config
sed -i "s/#Server/Server/g" /etc/pacman.d/mirrorlist
sed -i 's/#\(Storage=\)auto/\1volatile/' /etc/systemd/journald.conf
sed -i 's/#\(HandleSuspendKey=\)suspend/\1ignore/' /etc/systemd/logind.conf
sed -i 's/#\(HandleHibernateKey=\)hibernate/\1ignore/' /etc/systemd/logind.conf
sed -i 's/#\(HandleLidSwitch=\)suspend/\1ignore/' /etc/systemd/logind.conf

systemctl enable pacman-init.service choose-mirror.service
systemctl set-default graphical.target

# Start X session for shadow at startup
systemctl enable xlogin-plymouth@shadow

# Enable pulseaudio
systemctl enable pulseaudio

# Enable Bluetooth stack
systemctl enable bluetooth

# Enable NetworkManager service
systemctl enable NetworkManager

# Disable SUSPENDED on idle state in pulseaudio
sed -i 's/load-module module-suspend-on-idle/#load-module module-suspend-on-idle/' /etc/pulse/default.pa

# Fix a bug and start Zerotier service
sed -i 's/^After=.*$/After=network.target/' /lib/systemd/system/zerotier-one.service
systemctl enable zerotier-one

# Set the keyboard layout for Xorg
echo 'Section "InputClass"
    Identifier         "Keyboard Layout"
    MatchIsKeyboard    "yes"
    Option             "XkbLayout"  "fr"
    Option             "XkbVariant" "latin9" # accès aux caractères spéciaux plus logique avec "Alt Gr" (ex : « » avec "Alt Gr" w x)
EndSection' > /etc/X11/xorg.conf.d/00-keyboard.conf

# Enable tap to click
echo 'Section "InputClass"
       Identifier "tap-by-default"
       MatchIsTouchpad "on"
       MatchDriver "libinput"
       Option "Tapping" "on"
       Option "ScrollMethod" "twofinger"
EndSection' >> /etc/X11/xorg.conf.d/40-libinput.conf

# Add VirtualHere client to global
[ ! -f /bin/virtualhere ] && ln /usr/share/files/vhusbdx86_64 /bin/virtualhere

# Remove the custom repository
if [ ! -f /bin/pacman.conf.old ]; then
    mv /etc/pacman.conf /etc/pacman.conf.old
    head -n -4 /etc/pacman.conf.old > /etc/pacman.conf
fi

optimize_space
